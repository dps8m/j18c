#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <time.h>
#include <string.h>

#include "j18c.h"
#include "dn355.h"

//
// https://www.excamera.com/files/j1.pdf
//
// T is top of stack
// N is second item on stack
// R is top of return stack
// data stack is 33 16-bit words
// return stack is 33 16-bit words
// 13 bit program counter
//
// Memory is 16 bits wide
// Addresses 0-16383 are RAM
// 16384-32767 are memory-mapped I/O
// 
//   1nnnnnn  Literal
//   000nnnn  Jump
//   001nnnn  Conditional jump 
//   010nnnn  Call
//   011xxxx  ALU
//
//
//   15 14 13     12  11-8      7     6       5  4       3-2       1-0
//   17 16 15     14  13-10     9     8       7  6       5-4       3-2  1  0
//    0  1  1  R->PC     T'  T->N  T->R  N->[T]  x  rstack+-  dstack+-  x  x
//
//   T'
//    0      T
//    1      N
//    2      T + N
//    3      T & N
//    4      T | N
//    5      T ^ N
//    6      ~T
//    7      N = T
//    8      N < T
//    9      N >> T
//   10      T - 1
//   11      R
//   12      [T]
//   13      N << T
//   14      depth
//   15      N u< T

#define ALU_R_TO_PC 14
#define ALU_R_TO_PC_MASK (1u << ALU_R_TO_PC)
#define ALU_TPRIME 10
#define ALU_TPRIME_MASK (017u << ALU_TPRIME)
#define ALU_T_TO_N 9
#define ALU_T_TO_N_MASK (1u << ALU_T_TO_N)
#define ALU_T_TO_R 8
#define ALU_T_TO_R_MASK (1u << ALU_T_TO_R)
#define ALU_N_TO_M 7
#define ALU_N_TO_M_MASK (1u << ALU_N_TO_M)
#define ALU_RSTACK 4
#define ALU_RSTACK_MASK (3u << ALU_RSTACK)
#define ALU_DSTACK 2
#define ALU_DSTACK_MASK (3u << ALU_DSTACK)

#define DRSTK(i) (((i) & ALU_RSTACK_MASK) >> ALU_RSTACK)
#define DDSTK(i) (((i) & ALU_DSTACK_MASK) >> ALU_DSTACK)

#define IO_MASK 0xc000

#define RAM_SIZE  (1 << 14)  // 15 bits.
typedef uint16_t byte;
static byte ram[RAM_SIZE];
static cell insn;
static cell dsp; // data stack ptr; word address
static cell rsp;
static cell pc;  // Word Address !!!
static cell dstack[32];
static cell rstack[32];
static cell st0;
static bool run;

#define EVEN(a) ((a) & 0x3fffe)


#ifdef TRACE
bool trace = true;
static FILE * listfp;
static FILE * logfp;

static void listsrc (cell address) {
  fseek (listfp, 0, SEEK_SET);
  while (! feof (listfp)) {
    char buf[81];
    char prev[81];
    fgets (buf, 81, listfp);
    unsigned int addr, data;

    if (sscanf (buf, "%o %o", & addr, & data) == 2) {
      if (addr == address) {
        if (prev[0] == '\\')
          fprintf (logfp, "     %s", prev);
        fprintf (logfp, "     %s\n", buf);
        return;
      }
    }
    strcpy (prev, buf);
  }
  return;
}
#endif

//
// I/O memory
//

#define RS232_TXD 0x5000
#define RS232_RXD 0x5002
#define MULT_A 0x6100
#define MULT_B 0x6102
#define MULT_P 0x6104
#define J1HALT 0x6200

// Multipler
static cell mult_a;
static cell mult_b;
static cell mult_p;

// keypress
// https://web.archive.org/web/20180401093525/http://cc.byexamples.com/2007/04/08/non-blocking-user-input-in-loop-without-ncurses/

static void nonblock (bool state) {
  struct termios ttystate;
  //get the terminal state
  tcgetattr(STDIN_FILENO, &ttystate);
  if (state) {
    ttystate.c_lflag &= ~ECHO;
    //turn off canonical mode
    ttystate.c_lflag &= ~ICANON;
    //minimum of number input read.
    ttystate.c_cc[VMIN] = 1;
  } else {
    //turn on canonical mode
    ttystate.c_lflag |= ICANON;
  }
  //set the terminal attributes.
  tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}

static int kbhit (void) {
  struct timeval tv;
  fd_set fds;
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  FD_ZERO ( &fds);
  FD_SET (STDIN_FILENO, & fds); //STDIN_FILENO is 0
  select (STDIN_FILENO + 1, & fds, NULL, NULL, & tv);
  return FD_ISSET (STDIN_FILENO, & fds);
}

static cell io_din (cell address) {
#ifdef TRACE
  if (trace) fprintf (logfp, "io_din %06o\n", address);
#endif
  if (address == RS232_RXD) {
    if (kbhit ()) {
     //return getchar() | 0x20000;
     int c = getchar();
     return c | 0x20000;
    }
    return 0;
  }
  if (address == MULT_P)
    return (cell) ((mult_p >> 18) & 0x3ffff);
  if (address == MULT_P + 2)
    return (cell) ((mult_p >>  0) & 0x3ffff);
  cell data;
  if (dn355_io_din (address, & data))
    return data;
  fprintf (stderr, "io_din %06o\n", address);
exit (1);
  return 0;
}

static void io_dout (cell address, cell data) {
#ifdef TRACE
  if (trace) fprintf (logfp, "io_dout %06o %06o\n", address, data);
#endif
  if (address == J1HALT) {
    run = false;
    return;
  }
  if (address == RS232_TXD) {
    putchar (data);
    return;
  }
  if (address == MULT_A) {
    mult_a = data;
    mult_p = ((dcell) mult_a) * ((dcell) mult_b);
    return;
  }
  if (address == MULT_B) {
    mult_b = data;
    mult_p = ((dcell) mult_a) * ((dcell) mult_b);
//printf ("%06o * %06o = %012o\n", mult_a, mult_b, mult_p);
    return;
  }
  if (dn355_io_dout (address, data))
    return;
   fprintf (stderr, "io_dout %06o %06o\n", address, data);
}

static inline byte ramrb (cell address) {
//printf ("ramrb %o %o\n", address, ram[address]);
//printf ("ramrb %o %o\n", address, ((ram[address] & 01) << 8) | ((ram[address] >> 8) & 0377));
  uint8_t * p = (uint8_t *) & ram[address];
  // return ((ram[address] & 01) << 8) | ((ram[address] >> 8) & 0377);
  uint16_t hi = (uint16_t) p[0];
  uint16_t lo = (uint16_t) p[1];
  return (hi << 8) | lo;
}

static inline void ramwb (cell address, byte data) {
  // ram [address] = ((data & 1) << 8) | ((data >> 8) & 0377);
  uint8_t * p = (uint8_t *) & ram[address];

  p[0] = ((uint8_t) ((data >> 8) & 0001));
  p[1] = ((uint8_t) ((data >> 0) & 0377));
//printf ("ramwb %o %o %o\n", address, data, ram[address]);
}

static inline cell ramr (cell address) {
#define HLD 0x4ee
#define PAD 0x4f0
#define PADx 0x542
  // if (address >= PAD && address <= PADx) printf ("rd %06o:%06o\n", address, (((cell) ramrb (EVEN (address))) << 9) | (((cell) ramrb (EVEN (address) + 1))));
#ifdef TRACE
  if (trace)
    fprintf (logfp, "read  %06o %06o\n", address, (((cell) ramrb (EVEN (address))) << 9) | (((cell) ramrb (EVEN (address) + 1))));
#endif
  return (((cell) ramrb (EVEN (address))) << 9) | (((cell) ramrb (EVEN (address) + 1)));
}

static inline void ramw (cell address, cell data) {
  // if (address >= PAD && address <= PADx) printf ("wr %06o:%06o\n", address, data);
  ramwb (EVEN (address), (data >> 9) & 0x1ff);
  ramwb (EVEN (address) + 1, data & 0x1ff);
#ifdef TRACE
  if (trace)
    fprintf (logfp, "write %06o %06o %02o %02o %02o %02o\n", address, data, ram[address], ram[address + 1], ram[address + 2], ram[address + 3]);
#endif
}

static inline void wr (cell address, cell data) {
 if (address & IO_MASK)
   io_dout (address, data);
 else
   ramw (address, data);
}

static inline cell rd (cell address) {
 if (address & IO_MASK)
   return io_din (address);
 return ramr (address);
}

static long long icnt = 0;
#ifdef TRACE

static struct insEntry { cell insn; char * nme; } insTab [] = {
    { 0356074, "=" },
    { 0314000, "invert" },
    { 0312014, "xor" },
    { 0300214, "!a" },
    { 0302014, "!b" },
    { 0330000, "@" },
    { 0301004, "dup" },
    { 0302434, ">r" },
    { 0303000, "swap" },
    { 0303004, "over" },
    { 0327064, "r>" },
    { 0320014, "<" },
    { 0306014, "and" },
    { 0304014, "+" },
    { 0324000, "1-" },
    { 0332014, "lshift" },
    { 0310014, "or" },
    { 0316014, "=" },
};
#define N_INS_TAB (sizeof (insTab) / sizeof (struct insEntry))

static char disbuf[1204];
//static char disbuf2[1204];
static char * dis (cell ins) {
  uint8_t opcode = (insn >> 15) & 3;
  bool is_lit = !! (insn & 0x20000);
  bool is_bra = !! (opcode == 0);
  bool is_0bra = !! (opcode == 1);
  bool is_call = !! (opcode == 2);
  bool is_alu = !! (opcode == 3);

  //disbuf2[0] = 0;

  if (is_lit) {
    sprintf (disbuf, "LIT %06o", insn & MASK17);
    return disbuf;
  }
  if (is_bra) {
    sprintf (disbuf, "BRANCH %06o", (insn & MASK15) << 1);
    return disbuf;
  }
  if (is_0bra) {
    sprintf (disbuf, "0BRANCH %06o", (insn & MASK15) << 1);
    return disbuf;
  }
  if (is_call) {
    sprintf (disbuf, "CALL %06o", (insn & MASK15) << 1);
    return disbuf;
  }
  if (is_alu) {
    uint32_t op = insn & MASK15;
    bool semi = (!! (insn & ALU_R_TO_PC_MASK)) && (DRSTK (insn) == 3);
    sprintf (disbuf, "ALU");
    if (op & ALU_R_TO_PC_MASK) strcat (disbuf, " R->PC");
    uint8_t st0sel = (insn & ALU_TPRIME_MASK) >> ALU_TPRIME;
    switch (st0sel) {
      case  0: strcat (disbuf, " T"); break;
      case  1: strcat (disbuf, " N"); break;
      case  2: strcat (disbuf, " T+N"); break;
      case  3: strcat (disbuf, " T&N"); break;
      case  4: strcat (disbuf, " T|N"); break;
      case  5: strcat (disbuf, " T^N"); break;
      case  6: strcat (disbuf, " ^T"); break;
      case  7: strcat (disbuf, " N=T"); break;
      case  8: strcat (disbuf, " N<T"); break;
      case  9: strcat (disbuf, " N>>T"); break;
      case 10: strcat (disbuf, " T-1"); break;
      case 11: strcat (disbuf, " R"); break;
      case 12: strcat (disbuf, " [T]"); break;
      case 13: strcat (disbuf, " N<<T"); break;
      case 14: strcat (disbuf, " depth"); break;
      case 15: strcat (disbuf, " Nu<T"); break;
    }
    if (op & ALU_T_TO_N_MASK) strcat (disbuf, " T->N");
    if (op & ALU_T_TO_R_MASK) strcat (disbuf, " T->R");
    if (op & ALU_N_TO_M_MASK) strcat (disbuf, " N->[T]");
    uint8_t dd2 = (insn & ALU_DSTACK_MASK) >> ALU_DSTACK;
    switch (dd2) {
      case 0: break;
      case 1: strcat (disbuf, " dsp+1"); break;
      case 2: strcat (disbuf, " dsp-2"); break;
      case 3: strcat (disbuf, " dsp-1"); break;
    }
    uint8_t rd2 = (insn & ALU_RSTACK_MASK) >> ALU_RSTACK;
    switch (rd2) {
      case 0: break;
      case 1: strcat (disbuf, " rsp+1"); break;
      case 2: strcat (disbuf, " rsp-2"); break;
      case 3: strcat (disbuf, " rsp-1"); break;
    }
    cell insnx = insn;
    if (semi)
      insnx = insn & ~(ALU_RSTACK_MASK | ALU_R_TO_PC_MASK);
    strcat (disbuf, " \"");
    int i;
    for (i = 0; i < N_INS_TAB; i ++)
      if (insnx == insTab[i].insn) {
        strcat (disbuf, insTab[i].nme);
        break;
      }
    if (i >= N_INS_TAB) {
      strcat (disbuf, "???");
      char foo[32];
      sprintf (foo, "%06o", insnx);
      strcat (disbuf, foo);
     }
     if (semi)
      strcat (disbuf, ";");
    strcat (disbuf, "\"");
    // if (semi)
      // op = op & ~(ALU_RSTACK_MASK | ALU_R_TO_PC_MASK);
    // if (op == 0x008c) {  //  0 0000 0 0 1 0 00 11   T':N->[T] dstack -1
      // strcat (disbuf, "\"!\"(1)");
    // } else if (op == 0x040c) {  //  1 0001 0 0 0 0 11 11   R->PC T':N  rstack -1 dstack -1
      // strcat (disbuf, "\"!\"(2)");
    // } else if (op == 01004) {  //  0 0000 1 0 0 0 00 01  T':T  T->N     dstack 1
      // strcat (disbuf, "\"dup\"");
    // } else if (op == 03000) {  //  0 0001 1 0 0 0 00 00  T':N  T->N   
      // strcat (disbuf, "\"swap\"");
    // } else if (op == 020014) {  //  0 0001 1 0 0 0 00 00  T':N  T->N   
      // strcat (disbuf, "\"<\"");
    // } else if (op == 0x051c) {  //  0 0001 0 1 0 0 01 11  T':N  T->R   rstack 1 dstack -1
      // strcat (disbuf, "\">r\"");
    // } else if (op == 0x2e04) {  //  0 1011 1 0 0 0 00 01  T':R  T->N            dstack 1
      // strcat (disbuf, "\"r@\"");
    // } else if (op == 0x0c0c) {  //  0 0011 0 0 0 0 00 11  T':R  T&N             dstack -1
      // strcat (disbuf, "\"and\"");
    // } else if (op == 0x340c) {  //  0 1101 0 0 0 0 00 11  T':R  N<<T            dstack -1
      // strcat (disbuf, "\"lshift\"");
    // } else if (op == 0x3000) {  //  0 1100 0 0 0 0 00 00  T':[T]  
      // strcat (disbuf, "\"@\"");
    // } else if (op == 0x080c) {  //  0 0010 0 0 0 0 00 11  T':T+N dstack -1
      // strcat (disbuf, "\"+\"");
    // } else if (op == 0x0604) {  //  0 0001 1 0 0 0 00 01  T':N T->N dstack 1
      // strcat (disbuf, "\"over\"");
    // } else if (op == 0x2e34) {  //  0 1011 1 0 0 0 00 01  T':R T->N rstack -1 dstack 1 
      // strcat (disbuf, "\"r>\"");
    // } else if (op == 0x0000) {  //  0 1011 1 0 0 0 00 01  
      // strcat (disbuf, "\";\"");
    // } else if (op == 0x100c) {  //  0 0100 0 0 0 0 00 11  T':T|N dstack -1 
      // strcat (disbuf, "\"or\"");
    // } else if (op == 0x240c) {  //  0 0101 0 0 0 0 00 11  T':T<<N dstack -1 
      // strcat (disbuf, "\"rshift\"");
    // } else if (op == 0x2800) {  //  0 0110 0 0 0 0 00 00  T':T-1
      // strcat (disbuf, "\"1-\"");
    // } else if (op == 0x1e04) {  //  0 0111 1 0 0 0 00 01  T':N=T T->N dsp+1
      // strcat (disbuf, "\"2dup=\"");
#if 0
    if (insn == 0356074) {  
      strcat (disbuf, "\"=\"");
    } else if (insn == 0314000) {  
      strcat (disbuf, "\"invert\"");
    } else if (insn == 0312014) {  
      strcat (disbuf, "\"xor\"");
    } else if (insn == 0354060) {  
      strcat (disbuf, "\"xor;\"");
    } else if (insn == 0300214) {  
      strcat (disbuf, "\"!a\"");
    } else if (insn == 0302014) {  
      strcat (disbuf, "\"!b\"");
    } else if (insn == 0342074) {  
      strcat (disbuf, "\"!b;\"");
    } else if (insn == 0330000) {  
      strcat (disbuf, "\"@\"");
    } else if (insn == 0301004) {  
      strcat (disbuf, "\"dup\"");
    } else
      strcat (disbuf, "\"???\"");
#endif
    // if (semi && op != 0)
      // strcpy (disbuf2, "\";\"");
    return disbuf;
  }
  sprintf (disbuf, "???");
  return disbuf;
}
#endif

// uint18 to signed 32
#define SIGNED(u18)  (((u18) & SIGN18) ? ((int32_t) (((uint32_t) (u18)) | 0377777000000)) : ((int32_t) (u18)))

static void j18Cycle (void) {

  // RAM port A read insn
  insn = ramr (pc * 2);

#ifdef TRACE
  if (trace) {
    fprintf (logfp, "%08lld %06o:%06o %s", icnt, pc << 1, insn, dis (insn));
    fprintf (logfp, " [%06o", st0);
    for (int p = ((int) dsp); p >= 0; p --)
      fprintf (logfp, " %06o", dstack[p]);
    fprintf (logfp, "]");
    fprintf (logfp, " [");
    for (int p = ((int) rsp); p >= 0; p --)
      if (p == ((int) rsp))
        fprintf (logfp, "%06o", rstack[p]);
      else
        fprintf (logfp, " %06o", rstack[p]);
    fprintf (logfp, "]");
    fprintf (logfp, "\n");
    //if (strlen (disbuf2))
      //fprintf (logfp, "               ALU %s\n", disbuf2);
    listsrc (pc << 1);
  }
#endif

  pc += 1;

  uint8_t opcode = (insn >> 15) & 3;
  bool is_lit = !! (insn & 0x20000);
  bool is_bra = !! (opcode == 0);
  bool is_0bra = !! (opcode == 1);
  bool is_call = !! (opcode == 2);
  bool is_alu = !! (opcode == 3);

  if (is_lit) {
    dsp = (dsp + 1) % 32;
    dstack[dsp] = st0;
    st0 = insn & MASK17;
  } else if (is_alu) {
    uint8_t st0sel = (insn & ALU_TPRIME_MASK) >> ALU_TPRIME;
    cell st1 = dstack[dsp];
    cell st0_;
    switch (st0sel) {
      case  0: st0_ = st0;                                             break; // T
      case  1: st0_ = st1;                                             break; // N
      case  2: st0_ = st0 + st1;                                       break; // T + N
      case  3: st0_ = st0 & st1;                                       break; // T & N
      case  4: st0_ = st0 | st1;                                       break; // T | N
      case  5: st0_ = st0 ^ st1;                                       break; // T ^ N
      case  6: st0_ = ~st0;                                            break; // ~T
      case  7: st0_ = st0 == st1 ? 0x3ffff : 0;                        break; // T = N
      case  8: st0_ = (SIGNED (st1) < SIGNED (st0)) ? 0x3ffff : 0;     break; // T < N
      case  9: st0_ = st1 >> (st0 & 0xf);                              break; // T >> N
      case 10: st0_ = st0 - 1;                                         break; // T - 1
      case 11: st0_ = rstack[rsp];                                     break; // R
      case 12: st0_ = rd (st0);                                        break; // [T]
      case 13: st0_ = st1 << (st0 & 0xf);                              break; // T << N
      case 14: st0_ = (rsp << 8) | dsp;                                break; // depth
      case 15: st0_ = st1 < st0 ? 0x3ffff : 0;                         break; // T u< N
    }
    st0_ &= MASK18;

    uint8_t dd2 = (insn & ALU_DSTACK_MASK) >> ALU_DSTACK;
    int8_t dd = dd2 == 0 ? 0 : dd2 == 1 ? 1 : dd2 == 2 ? -2 : /* dd2 == 3 */ -1;
    cell dsp_ = (dsp + dd) % 32;

    uint8_t rd2 = (insn & ALU_RSTACK_MASK) >> ALU_RSTACK;
    int8_t rd = rd2 == 0 ? 0 : rd2 == 1 ? 1 : rd2 == 2 ? -2 : /* rd2 == 3 */ -1;
    cell rsp_ = (rsp + rd) % 32;

    if (insn & ALU_N_TO_M_MASK)
      wr (EVEN (st0_), st1);

    if (insn & ALU_T_TO_N_MASK)
      dstack[dsp_] = st0;

    if (insn & ALU_T_TO_R_MASK)
      rstack[rsp_] = st0;

    if (insn & ALU_R_TO_PC_MASK)
      //pc = rstack[rsp];
      pc = rstack[rsp] >> 1;

    st0 = st0_;
    rsp = rsp_;
    dsp = dsp_;

  } else if (is_bra) { // jump
    pc = (insn & MASK15);
  } else if (is_0bra) { // conditional branch
    if (st0 == 0)
      pc = (insn & MASK15);
    st0 = dstack [dsp];
    dsp = (dsp - 1) % 32;
  } else if (is_call) { // call
    rsp = (rsp + 1) % 32;
    //rstack[rsp] = pc;
    rstack[rsp] = pc << 1;
    pc = (insn & MASK15);
  }
  icnt ++;
}

int main (int argc, char * argv[]) {

  memset (ram, 0, sizeof (ram));

  int fd = open ("j18.bin", O_RDONLY);
  if (fd < 0) { perror ("j18.bin"); exit (1); }
  ssize_t n = read (fd, ram, sizeof (ram));
  printf ("%ld bytes read from j18.bin\n", n);
  close (fd);

  nonblock (true);
  setvbuf (stdout, NULL, _IONBF, 0);

#ifdef TRACE
  if (trace) {
    logfp = fopen ("j18c.log", "w");
    if (! logfp) { perror ("j18c.log"); exit (1); }
    setvbuf (logfp, NULL, _IONBF, 0);
    listfp = fopen ("j18.lst", "r");
    if (! listfp) { perror ("j18.lst"); exit (1); }
  }
#endif

  icnt = 0;
  // clock_t start = clock ();
  pc = 0;
  dsp = 0;
  st0 = 0;
  rsp = 0;

  run = true;
  while (run) {
    j18Cycle ();
  }
  nonblock (false);
  // clock_t stop = clock ();
  // double cpu_time_used = ((double) (stop - start)) / CLOCKS_PER_SEC;
  // fprintf (stderr, "%f\n", cpu_time_used);
  // fprintf (stderr, "%lld\n", icnt);
  // fprintf (stderr, "%f\n", (double) icnt / cpu_time_used);
}
