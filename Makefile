#CFLAGS = -Wall -O0 -g3 -DTRACE
#CFLAGS = -Wall -O0 -g3 
CFLAGS = -Wall -O3 
all : j18c j18.mem j18.bin

clean:
	-rm j18c j18.mem j18.bin j18.lst j18c.log

j18c : j18c.c j18c.h dn355.c dn355.h
	$(CC) $(CFLAGS) j18c.c dn355.c -o j18c

FSES = main.fs crossj18.fs basewords.fs hwdefs.fs nuc.fs dn355.fs

j18.mem j18.bin: $(FSES) Makefile
	gforth -e 'include main.fs bye'
