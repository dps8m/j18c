( Main for WGE firmware                      JCB 13:24 08/24/10)

\ warnings off
\ require tags.fs

include crossj18.fs
meta
    : TARGET? 1 ;
    : build-debug? 1 ;

include basewords.fs
target
include hwdefs.fs

4 org
module[ everything"
include nuc.fs

\ 33333333 / 115200 = 289, half cycle is 144

: pause144
    d# 0 d# 45
    begin
        1-
        2dup=
    until
    2drop
;

: serout ( u -- )
  RS232_TXD !
;

: frac ( ud u -- d1 u1 ) \ d1+u1 is ud
    >r 2dup d# 1 r@ m*/ 2swap 2over r> d# 1 m*/ d- drop ;
: .2  s>d <# # # #> type ;

code in end-code
: on ( a -- ) d# 1 swap ! ;
code out end-code
: off ( a -- ) d# 0 swap ! ;


include dn355.fs

: main
    decimal
    ['] serout 'emit !

    dn355

    d# 1 j18halt ! 
;


]module

0 org

code 0jump
    \ h# 3e00 ubranch
    main ubranch
    main ubranch
end-code

meta

hex
: create-output-file w/o create-file throw to outfile ;

\ .mem is a memory dump formatted for use with the Xilinx
\ data2mem tool.
s" j18.mem" create-output-file
:noname
    s" @ 20000" type cr
    4000 0 do i t@ s>d <# # # # # #> type cr 2 +loop
; execute

\ .bin is a big-endian binary memory dump
s" j18.bin" create-output-file
\ :noname 4000 0 do i t@ dup 8 rshift emit emit 2 +loop ; execute
\ :noname
\    4000 0 do
\      i t@
\      dup  8 rshift ff and emit
\      dup           ff and emit
\      dup 18 rshift ff and emit
\      dup 10 rshift ff and emit
\      drop
\    2 +loop ; execute

:noname 4000 0 do i tc@ dup 8 rshift emit emit loop ; execute

\ .lst file is a human-readable disassembly 
s" j18.lst" create-output-file
d# 0
h# 2000 disassemble-block
